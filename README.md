# powerUp case
The purpose of the case is to generate a matrix for tracking assumptions. 

## Serve Storybook
```
yarn storybook:serve
```

### Run Storybook
```
yarn lint
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```


### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

import '!style-loader!css-loader!sass-loader!../assets/scss/_variables.scss';
import '!style-loader!css-loader!sass-loader!../assets/scss/_reset.scss';
import Matrix from '@/components/matrix/Matrix.vue';

export default {
  title: 'Matrix',
}

export const basicMatrix = () => ({
  components: { Matrix },
  template: '<matrix></matrix>',
});

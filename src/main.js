import Vue from 'vue'
import App from './App.vue'
import VueLodash from 'vue-lodash'
import isEmpty from 'lodash/isEmpty'
 
Vue.use(VueLodash, { 
  lodash: {
    isEmpty
  } 
})

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
